﻿using System;
using System.Linq;


namespace Project
{
    class Program
    {
        static void Main(string[] args)
        {
            Note firstNote, secondNote, thirdNote;

            firstNote = new Note();
            secondNote = new Note("usage of constructor for creating new note with text only");
            thirdNote = new Note("note with full info", "Valentin", 5);


            Console.WriteLine(firstNote.GetAuthor());
            Console.WriteLine(firstNote.GetText());

            Console.WriteLine(secondNote.GetAuthor());
            Console.WriteLine(secondNote.GetText());

            Console.WriteLine(thirdNote.GetAuthor());
            Console.WriteLine(thirdNote.GetText());

            Console.WriteLine(firstNote);

            Console.WriteLine(thirdNote.ToString());
            Console.WriteLine(thirdNote);


            TimestampedNote note;

            note = new TimestampedNote("timestamped note with current time info", "Valentin", 1);

            note.ToString();
            Console.WriteLine(note);
            //=========================================TO DO PART============================


            ToDo list = new ToDo();


            list.AddNote(firstNote);
            list.AddNote(secondNote);
            list.AddNote(thirdNote);


                for (int i = 0; i < 3; i++)
                {

                    Console.WriteLine("Input note text:");
                    string text = Console.ReadLine();

                    Console.WriteLine("Input note author:");
                    string author = Console.ReadLine();

                    Console.WriteLine("Input note importance:");
                    string importance = Console.ReadLine();

                    Note newNote = new Note(text, author, Int32.Parse(importance));

                    list.AddNote(newNote);
                }

            


            list.SortByImportance();
            list.RemoveEmpty();


            Console.WriteLine(list.ToString());

            list.RemoneHighestImportance();

            list.SortByImportance();

            Console.WriteLine(list.ToString());


         //console.WriteLine("\n get note with index 1 \n" + list.GetToDo(1).ToString());
           



        }
    }
}
