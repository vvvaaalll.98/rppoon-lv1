﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project
{
    /*  
 2.
     Kreirajte klasu koja predstavlja zabilješku. Klasa treba omogućiti bilježenje teksta zabilješke, autora
 zabilješke te razine važnosti zabilješke. Poštovati pravila enkapsulacije, a omogućiti izmjenu samo
 teksta i razine važnosti, jednom zadan autor ne smije se izmijeniti.


 3.
    Kreirajte konstruktore za zabilješke (barem tri konstruktora). Napravite tri zabilješke koristeći
 različite konstruktore. Ispišite autore i tekst zabilješki.

         */

    class Note
    {
        private String text;
        private readonly String author; //ovako ili samo private setter
        private int importance;


        public string GetText() { return this.text; }
        public string GetAuthor() { return this.author; }
        public int GetImportance() { return this.importance; }

        public void SetText(string text) { this.text = text; }
        public void SetImportance(int importance) { this.importance = importance; }


        public string Text {

            get { return this.text; }
            set { this.text = value; }
        }

        public string Author {

            get { return this.author; }
        }

        public int Importance {

            get { return this.importance; }
            set { this.importance = value; }
        }

        public Note() {

            this.text = "Note is empty.";
            this.author = "Unknown.";
            this.importance = 0;
        }

        public Note(string text) {

            this.text = text;
            this.author = "Unknown.";
            this.importance = 0;
        }

        public Note(string text, string author) {

            this.text = text;
            this.author = author;
            this.importance = 0;

        }

        public Note(string text, string author, int importance)
        {

            this.text = text;
            this.author = author;
            this.importance = importance;

        }



        public override string ToString()
        {
            return this.importance + ", " + this.text + ", " + this.author ;
        }


    }
}
