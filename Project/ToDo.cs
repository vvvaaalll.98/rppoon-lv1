﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


/*
 
     Kreirajte klasu koja predstavlja ToDo listu koja omogućuje praćenje zadataka (zabilješki). 
     Klasa mora omogućiti dodavanje i uklanjanje (obavljanje) zadataka preko javnog sučelja, dohvaćanje pojedinih bilješki te ispis cijele ToDo liste (ne vezati uz konzolu!). 
     Kreirajte objekt navedene klase i napunite svoju ToDo listu zabilješkama čije ćete vrijednosti unijeti s konzole (barem tri). Nakon toga ispišite sadržaj ToDo liste na ekran,
     obavite sve zadatke s najvišom razinom prvenstva i ponovno ispišite sadržaj ToDo liste.
Paziti na enkapsulaciju, ne vezati izlaz uz konzolu, voditi računa o imenovanju!
     */

namespace Project
{
    class ToDo
    {

       private List<Note> toDo = new List<Note>();
      //  private List <Note>  toDo;

        //public List<Note> ToDo { get; private set; }

        public List<Note> GetToDo() {

            return toDo;    //returns list
        }

        public Note GetToDoNote(int n)
        {
            if (n <= this.toDo.Count()) return (toDo[n]);

            else return null;
        }

      




      

        public void AddNote(Note newNote) {

            this.toDo.Add(newNote);
        }




        public void RemoveEmpty() { //removes notes with no significant importance (0)
            for (int i = 0; i < this.toDo.Count; i++) {
                if (this.toDo[i].Importance == 0)
                {

                    this.toDo.RemoveAt(i);
                    i -= 1;
                }
            }
        }

        
        public void RemoneHighestImportance() { //s pretpostavkom da lista nije sortirana

            int min = this.toDo[0].GetImportance();

            for (int i = 0; i < this.toDo.Count(); i++) {

                if (this.toDo[i].GetImportance() < min) min = this.toDo[i].GetImportance();
            
            }

            for (int i = 0; i < this.toDo.Count(); i++)
            {

                if (this.toDo[i].GetImportance() == min) { 
                    
                    RemoveNote(i);
                    i -= 1;
                }
                    
            }

        }

        public void  RemoveNote(int n) {

            this.toDo.RemoveAt(n);

        }


        public void SortByImportance()
        {    //1 most important, higher number-> lesser importance


            for (int i = 0; i < this.toDo.Count - 1; i++) {
                for (int j = 0; j < this.toDo.Count - i - 1; j++) {

                    if (this.toDo[j].Importance > this.toDo[j + 1].Importance) {

                        Note temp = new Note();
                        temp = this.toDo[j];

                        this.toDo[j] = this.toDo[j + 1];
                        this.toDo[j + 1] = temp;

                    }
                }
            }
            
        }


        public override string ToString()
        {
            string output ="";
            for (int i = 0; i < this.toDo.Count; i++) {

               

                output += this.toDo[i].ToString() + "\n";
            }

             return output;

        }


    }
}
