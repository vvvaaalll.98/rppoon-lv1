﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project
{
    class TimestampedNote : Note
    {
        private DateTime time;

        public void UpdateTime() {  //setter??
            
            this.time = DateTime.Now;
        }

        public void SetTime(DateTime time) {

            this.time = time;
        }

        public TimestampedNote(string text, string author, int importance) : base (text,author,importance){

            this.time = DateTime.Now;     
            
        }


        public TimestampedNote(string text, string author, int importance, DateTime time) : base(text, author, importance)
        {

            this.time = time;

        }


        public override string ToString()
        {
            return this.Importance + ", " + this.Text + ", " + this.Author + ", " + this.time;
        }

    }
}
